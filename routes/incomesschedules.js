const express = require('express');

const incomesSchedules = express.Router();
const checkAuth = require('../middleware/checkAuth');
const incomesSchedulesController = require('../controllers/incomesScheduleController');

// Get all result
incomesSchedules.get('/', checkAuth, incomesSchedulesController.getIncomesSchedules);
incomesSchedules.get('/waiting', checkAuth, incomesSchedulesController.getIncomesSchedulesWaiting);
incomesSchedules.post('/', checkAuth, incomesSchedulesController.addIncomesDate);

module.exports = incomesSchedules;