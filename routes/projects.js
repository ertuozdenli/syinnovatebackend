const express = require('express');

const projects = express.Router();
const checkAuth = require('../middleware/checkAuth');
const projectController = require('../controllers/projectController');

// Get all result
projects.get('/', checkAuth, projectController.getProjects);
projects.get('/:id', checkAuth, projectController.getProject);
projects.post('/', checkAuth, projectController.createProject);
projects.put('/:id', checkAuth, projectController.updateProject);
projects.delete('/:projectId/removeservice/:serviceId', checkAuth, projectController.removeServiceToProject);
projects.delete('/', checkAuth, projectController.deleteProject);
projects.post('/:id/addService', checkAuth, projectController.addServiceToProject);
projects.post('/:projectId/addIncomesSchedules', checkAuth, projectController.addIncomesSchedulesToProject);

module.exports = projects;