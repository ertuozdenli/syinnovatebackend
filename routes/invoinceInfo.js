const express = require('express');

const invoinceinfos = express.Router();
const checkAuth = require('../middleware/checkAuth');
const invoinceInfoController = require('../controllers/invoinceInfoController');

// Get all result
invoinceinfos.get('/', checkAuth, invoinceInfoController.getInvoinceInfos);
invoinceinfos.post('/', checkAuth, invoinceInfoController.createInvoinceInfo);
invoinceinfos.put('/:id', checkAuth, invoinceInfoController.updateInvoinceInfo);
invoinceinfos.delete('/:id', checkAuth, invoinceInfoController.deleteInvoinceInfo);
module.exports = invoinceinfos;