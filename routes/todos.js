const express = require('express');

const todos = express.Router();
const checkAuth = require('../middleware/checkAuth');
const checkOwner = require('../middleware/checkOwner');
const todoController = require('../controllers/todoController');

// Get all result
todos.get('/', checkAuth, todoController.getTodos);
todos.post('/', checkAuth, todoController.createTodo);
todos.get('/:id', checkAuth, checkOwner, todoController.getTodoDetails);
todos.put('/:id', checkAuth, todoController.updateTodo);
todos.delete('/', checkAuth, todoController.deleteTodo);

module.exports = todos;