const express = require('express');

const services = express.Router();
const checkAuth = require('../middleware/checkAuth');
const serviceController = require('../controllers/serviceController');

// Get all result
services.get('/', checkAuth, serviceController.getServices);
services.get('/:id', checkAuth, serviceController.getService);
services.post('/', checkAuth, serviceController.createService);
services.put('/:id', checkAuth, serviceController.updateService);
services.delete('/', checkAuth, serviceController.deleteService);

module.exports = services;