const express = require('express');

const expense = express.Router();
const checkAuth = require('../middleware/checkAuth');
const expenseController = require('../controllers/expenseController');

// Get all result
expense.get('/', checkAuth, expenseController.getExpenses);
expense.post('/', checkAuth, expenseController.addExpense);
expense.put('/:id', checkAuth, expenseController.updateExpense);
expense.delete('/', checkAuth, expenseController.removeExpense);
expense.patch('/:id', checkAuth, expenseController.updateStatus);

module.exports = expense;
