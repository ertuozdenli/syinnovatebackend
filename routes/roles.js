const express = require('express');

const roles = express.Router();
const checkAuth = require('../middleware/checkAuth');
const roleController = require('../controllers/roleController');

// Get all result
roles.get('/', checkAuth, roleController.getRoles);
roles.post('/', checkAuth, roleController.createRole);
roles.put('/:id', checkAuth, roleController.updateRole);

module.exports = roles;