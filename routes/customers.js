const express = require('express');
const customers = express.Router();
const customerController = require('../controllers/customerController');
const checkAuth = require('../middleware/checkAuth');

customers.get('/', checkAuth, customerController.getCustomers);
customers.get('/:id', checkAuth, customerController.getCustomer);
customers.post('/', checkAuth, customerController.addCustomer);
customers.delete('/', checkAuth, customerController.deleteCustomer);
customers.put('/:id', checkAuth, customerController.updateCustomer);

module.exports = customers;