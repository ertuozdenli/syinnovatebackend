const express = require('express');
const offers = express.Router();
const offerController = require('../controllers/offerController');
const checkAuth = require('../middleware/checkAuth');

offers.get('/', checkAuth, offerController.getOffers);
offers.get('/:id', checkAuth, offerController.getOffer);
offers.post('/', checkAuth, offerController.createOffer);
offers.delete('/', checkAuth, offerController.deleteOffer);
offers.put('/:id', checkAuth, offerController.updateOffer);
offers.post('/:id/converttoproject', checkAuth, offerController.convertOfferToProject);

module.exports = offers;
