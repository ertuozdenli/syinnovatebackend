const express = require('express');

const authorizedPersons = express.Router();
const checkAuth = require('../middleware/checkAuth');
const authorizedPersonController = require('../controllers/authorizedPersonController');

// Get all result
authorizedPersons.get('/', checkAuth, authorizedPersonController.getAuthorizedPersons);
authorizedPersons.post('/', checkAuth, authorizedPersonController.addAuthorizedPerson);
authorizedPersons.put('/:id', checkAuth, authorizedPersonController.updateAuthorizedPerson);
authorizedPersons.delete('/', checkAuth, authorizedPersonController.deleteAuthorizedPerson);

module.exports = authorizedPersons;