const express = require('express');

const incomes = express.Router();
const checkAuth = require('../middleware/checkAuth');
const incomeController = require('../controllers/incomeController');

// Get all result
incomes.get('/', checkAuth, incomeController.getIncomes);
incomes.post('/', checkAuth, incomeController.addIncome);
incomes.put('/:id', checkAuth, incomeController.updateIncome);
incomes.delete('/', checkAuth, incomeController.removeIncome);

module.exports = incomes;