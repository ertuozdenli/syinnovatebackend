const express = require('express');

const taxs = express.Router();
const checkAuth = require('../middleware/checkAuth');
const taxController = require('../controllers/taxController');

// Get all result
taxs.get('/', checkAuth, taxController.getTaxs);
taxs.get('/:id', checkAuth, taxController.getTax);
taxs.post('/', checkAuth, taxController.createTax);
taxs.put('/:id', checkAuth, taxController.updateTax);
taxs.delete('/', checkAuth, taxController.deleteTaxs);

module.exports = taxs;
