const express = require('express');

const files = express.Router();
const checkAuth = require('../middleware/checkAuth');
const filesController = require('../controllers/filesController');
const fileUpload = require('../multerConfig');


// Get all result
files.get('/', checkAuth, filesController.getFiles);
files.post('/', checkAuth, fileUpload.uploadFiles.array('files', 10), filesController.addFiles);
files.delete('/', checkAuth, filesController.deleteFiles);

module.exports = files;