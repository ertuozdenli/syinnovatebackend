const express = require('express');

const users = express.Router();

const userContoller = require('../controllers/userController');
const checkAuth = require('../middleware/checkAuth');
const fileUpload = require('../multerConfig');

require('dotenv').config();

users.get('/', checkAuth, userContoller.main);
users.get('/profile/:id', checkAuth, userContoller.getUser);
users.put('/profile/:id', checkAuth, fileUpload.uploadImage.single('profileImg'), userContoller.updateUser);
users.post('/', checkAuth, userContoller.addUser);
users.post('/signin', userContoller.signin);
users.get('/checkauth', checkAuth, userContoller.checkAuth);
users.patch('/changepassword', checkAuth, userContoller.changePassword);

module.exports = users;