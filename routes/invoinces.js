const express = require('express');

const invoince = express.Router();
const checkAuth = require('../middleware/checkAuth');
const invoinceController = require('../controllers/invoinceController');

// Get all result
invoince.get('/', checkAuth, invoinceController.getInvoinces);
invoince.post('/', checkAuth, invoinceController.addInvoince);
invoince.put('/:id', checkAuth, invoinceController.updateInvoince);
invoince.delete('/', checkAuth, invoinceController.removeInvoinces);

module.exports = invoince;