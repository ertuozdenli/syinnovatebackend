'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('roles', [{
      name: 'Administrator',
      desc: 'Her yere müdahale edebiliyor',
      createdAt: '2019-11-27 17:18:11',
      updatedAt: '2019-11-27 17:18:11'
    },
    {
      name: 'Chef',
      desc: 'Finans akışını da görebiliyor',
      createdAt: '2019-11-27 17:18:11',
      updatedAt: '2019-11-27 17:18:11'
    },
    {
      name: 'Worker',
      desc: 'Sadece iş',
      createdAt: '2019-11-27 17:18:11',
      updatedAt: '2019-11-27 17:18:11'
    }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('roles', null, {});
  }
};