'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    //pass = test123

    return queryInterface.bulkInsert('users', [{
      name: 'Ertuğrul Özdenli',
      email: 'ertu@seksenyirmi.com',
      password: '$2b$10$mJ79BuIXzojw0wlCACHs.uehZF/UdX58VF6nNtoy.XyeNbDW9LlA6',
      roleID: 1,
      status: 1,
      createdAt: '2019-11-27 17:18:11',
      updatedAt: '2019-11-27 17:18:11'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('users', null, {});
  }
};