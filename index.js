const express = require('express');

const app = express();
const bodyParser = require('body-parser');

const todos = require('./routes/todos');
const users = require('./routes/users');
const customers = require('./routes/customers');
const roles = require('./routes/roles');
const invoinceInfos = require('./routes/invoinceInfo');
const authorizedpersons = require('./routes/authorizedpersons');
const taxs = require('./routes/taxs');
const services = require('./routes/services');
const projects = require('./routes/projects');
const incomesSchedules = require('./routes/incomesschedules');
const offers = require('./routes/offers');
const incomes = require('./routes/incomes');
const expenses = require('./routes/expenses');
const invoinces = require('./routes/invoinces');
const files = require('./routes/files');

app.use(bodyParser.urlencoded({
  extended: false,
}));

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH',
  });
  next();
});

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'up',
  });
});
app.use('/images', express.static('storage'));
app.use('/todos', todos);
app.use('/users', users);
app.use('/customers', customers);
app.use('/roles', roles);
app.use('/invoinceinfos', invoinceInfos);
app.use('/authorizedpersons', authorizedpersons);
app.use('/taxs', taxs);
app.use('/services', services);
app.use('/projects', projects);
app.use('/incomeschedules', incomesSchedules);
app.use('/offers', offers);
app.use('/incomes', incomes);
app.use('/expenses', expenses);
app.use('/invoinces', invoinces);
app.use('/files', files);

app.listen(8080, (err) => {
  if (err) {
    console.log(err);
  }
  console.log('Listening tatlım');
});