const jwt = require('jsonwebtoken');

const checkAuth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    jwt.verify(token, process.env.JWT_SECRET, (err) => {
      if (!err) {
        return next();
      }
      return res.status(401).json({
        message: 'Invalid Token',
      });
    });
  } catch (e) {
    return res.status(401).json({
      message: 'Auth Required',
    });
  }
};
module.exports = checkAuth;