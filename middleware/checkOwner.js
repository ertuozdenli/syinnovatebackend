const jwt = require('jsonwebtoken');
const models = require('../models');

const checkOwner = (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1];
  const decoded = jwt.decode(token);

  models.todo.findAll({
    where: {
      id: req.params.id,
    },
  }).then((result) => {
    if (result !== '') {
      if (result[0].userId === decoded.id) {
        return next();
      }
      return res.status(401).json({
        message: 'Unauthorized access',
      });
    }
    return res.status(404).json({
      message: 'Not Found',
    });
  });
};

module.exports = checkOwner;
