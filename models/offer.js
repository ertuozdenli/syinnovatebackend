'use strict';
module.exports = (sequelize, DataTypes) => {
  var offer = sequelize.define('offer', {
    name: DataTypes.STRING,
    desc: DataTypes.STRING,
    price: DataTypes.DECIMAL,
    startTime: DataTypes.DATE,
    deadline: DataTypes.DATE,
    status: DataTypes.INTEGER,
    period: DataTypes.BOOLEAN,
    incomesSchedulesData: DataTypes.STRING,
    servicesData: DataTypes.STRING
  }, {});
  offer.associate = function (models) {
    // associations can be defined here
    offer.belongsTo(models.customer);
  };
  return offer;
};