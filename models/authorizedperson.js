'use strict';
module.exports = (sequelize, DataTypes) => {
  var authorizedperson = sequelize.define('authorizedperson', {
    title: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.INTEGER,
    mail: DataTypes.STRING
  }, {
    tableName: 'authorizedpersons'
  });
  authorizedperson.associate = function (models) {
    // associations can be defined here
  };
  return authorizedperson;
};