module.exports = (sequelize, DataTypes) => {
  const todo = sequelize.define('todo', {
    head: DataTypes.STRING,
    body: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
  }, {});

  todo.associate = (models) => {
    todo.belongsTo(models.user);
  };
  return todo;
};