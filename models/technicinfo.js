'use strict';
module.exports = (sequelize, DataTypes) => {
  var technicInfo = sequelize.define('technicInfo', {
    hosting: DataTypes.TEXT,
    domain: DataTypes.TEXT,
    social: DataTypes.TEXT
  }, {});
  technicInfo.associate = function (models) {
    // associations can be defined here
    technicInfo.belongsTo(models.customer);
  };
  return technicInfo;
};