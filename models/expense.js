'use strict';
module.exports = (sequelize, DataTypes) => {
  var expense = sequelize.define('expense', {
    towho: DataTypes.STRING,
    desc: DataTypes.STRING,
    value: DataTypes.DECIMAL,
    date: DataTypes.DATE,
    status: DataTypes.BOOLEAN
  }, {});
  expense.associate = function(models) {
    // associations can be defined here
  };
  return expense;
};