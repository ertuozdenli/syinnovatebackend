'use strict';
module.exports = (sequelize, DataTypes) => {
  var customer = sequelize.define('customer', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    web: DataTypes.STRING,
    logo: DataTypes.STRING
  }, {});
  customer.associate = function (models) {
    // associations can be defined here
    customer.hasMany(models.invoinceInfo);
    customer.hasOne(models.technicInfo);
    customer.hasMany(models.authorizedperson);
    customer.hasMany(models.project);
    customer.hasMany(models.offer);
    customer.hasMany(models.file);
  };
  return customer;
};