'use strict';
module.exports = (sequelize, DataTypes) => {
  var role = sequelize.define('role', {
    name: DataTypes.STRING,
    desc: DataTypes.STRING
  }, {});
  role.associate = (models) => {
    // associations can be defined here
    // role.hasMany(models.user);
  };
  return role;
};