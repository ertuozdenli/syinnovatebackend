module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    profileImg: DataTypes.STRING
  }, {});
  user.associate = (models) => {
    user.hasMany(models.todo);
    user.belongsTo(models.role);
  };

  return user;
};