'use strict';
module.exports = (sequelize, DataTypes) => {
  var invoince = sequelize.define('invoince', {
    inv_no: DataTypes.INTEGER,
    date: DataTypes.DATE,
    servicesandprices: DataTypes.STRING,
    tax: DataTypes.DECIMAL,
    total: DataTypes.DECIMAL
  }, {});
  invoince.associate = function (models) {
    // associations can be defined here
    invoince.belongsTo(models.customer);
  };
  return invoince;
};