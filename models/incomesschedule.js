'use strict';
module.exports = (sequelize, DataTypes) => {
  var incomesSchedule = sequelize.define('incomesSchedule', {
    value: DataTypes.DECIMAL,
    date: DataTypes.DATE,
    status: DataTypes.INTEGER
  }, {});
  incomesSchedule.associate = function (models) {
    // associations can be defined here
    incomesSchedule.belongsTo(models.project);
  };
  return incomesSchedule;
};