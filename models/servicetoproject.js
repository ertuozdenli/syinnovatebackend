'use strict';
module.exports = (sequelize, DataTypes) => {
  var serviceToProject = sequelize.define('serviceToProject', {
    projectId: DataTypes.INTEGER,
    serviceId: DataTypes.INTEGER
  }, {});
  serviceToProject.associate = function(models) {
    // associations can be defined here
  };
  return serviceToProject;
};