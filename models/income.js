'use strict';
module.exports = (sequelize, DataTypes) => {
  var income = sequelize.define('income', {
    date: DataTypes.DATE,
    value: DataTypes.DECIMAL
  }, {});
  income.associate = function (models) {
    // associations can be defined here
    income.belongsTo(models.customer);
  };
  return income;
};