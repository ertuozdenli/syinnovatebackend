'use strict';
module.exports = (sequelize, DataTypes) => {
  var file = sequelize.define('file', {
    name: DataTypes.STRING,
    path: DataTypes.STRING
  }, {});
  file.associate = function (models) {
    // associations can be defined here
    file.belongsTo(models.customer);
  };
  return file;
};