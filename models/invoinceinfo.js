'use strict';
module.exports = (sequelize, DataTypes) => {
  var invoinceInfo = sequelize.define('invoinceInfo', {
    alias: DataTypes.STRING,
    name: DataTypes.STRING,
    taxOffice: DataTypes.STRING,
    taxNo: DataTypes.INTEGER,
    address: DataTypes.STRING
  }, {});
  invoinceInfo.associate = function (models) {
    invoinceInfo.belongsTo(models.customer);
  };
  return invoinceInfo;
};