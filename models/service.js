'use strict';
module.exports = (sequelize, DataTypes) => {
  var service = sequelize.define('service', {
    name: DataTypes.STRING,
    desc: DataTypes.STRING,
    defaultPrice: DataTypes.DECIMAL
  }, {});
  service.associate = function (models) {
    // associations can be defined here
    service.belongsTo(models.tax);
    service.belongsToMany(
      models.project, {
        through: 'serviceToProject'
      });
  };
  return service;
};
