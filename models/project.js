'use strict';
module.exports = (sequelize, DataTypes) => {
  var project = sequelize.define('project', {
    name: DataTypes.STRING,
    desc: DataTypes.STRING,
    price: DataTypes.DECIMAL,
    startTime: DataTypes.DATE,
    deadline: DataTypes.DATE,
    period: DataTypes.BOOLEAN
  }, {});
  project.associate = function (models) {
    // associations can be defined here
    project.belongsTo(models.customer);
    project.belongsToMany(models.service, {
      through: 'serviceToProject',
    });
    project.hasMany(models.incomesSchedule);
  };
  return project;
};