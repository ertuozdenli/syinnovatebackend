'use strict';
module.exports = (sequelize, DataTypes) => {
  var tax = sequelize.define('tax', {
    name: DataTypes.STRING,
    value: DataTypes.INTEGER
  }, {
    tableName: 'taxs'
  });
  tax.associate = function (models) {
    // associations can be defined here
  };
  return tax;
};