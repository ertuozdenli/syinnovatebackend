const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'storage')
  },
  filename: function (req, file, cb) {
    const filename = file.originalname.split('.').slice(0, -1).join('.').replace(/[^A-Z0-9]/ig, "-").toLowerCase();
    cb(null, filename + '-' + Date.now() + "." + file.mimetype.split("/")[1])
  },
});

exports.uploadImage = multer({
  storage: storage,
  fileFilter: function (req, file, cb) {
    const imageTypes = /jpeg|jpg|png/;
    const mimeResult = imageTypes.test(file.mimetype);
    let filetype = path.extname(file.originalname).toLowerCase().substr(1);
    filetype = new RegExp(filetype);
    const isEqual = filetype.test(file.mimetype);

    if (isEqual && mimeResult) {
      return cb(null, true);
    }
    return cb(null, false);
  },
  limits: {
    fileSize: 1024 * 16
  }
});

exports.uploadFiles = multer({
  storage: storage,
  fileFilter: function (req, file, cb) {
    const fileTypes = /jpeg|jpg|png|svg|pdf|doc|docx|xls|xlsx|xd/;
    const mimeResult = fileTypes.test(file.mimetype);
    let filetype = path.extname(file.originalname).toLowerCase().substr(1);
    filetype = new RegExp(filetype);
    const isEqual = filetype.test(file.mimetype);

    if (isEqual && mimeResult) {
      return cb(null, true);
    }
    return cb(null, false);
  },
  limits: {
    fileSize: 1024 * 512
  }
});