const models = require('../models');

exports.getInvoinces = (req, res) => {
  models.invoince.findAll({
    include: {
      all: true
    }
  }).then((result) => {
    res.status(200).json(result)
  }).catch(() => {
    res.status(500).send();
  })
}

exports.addInvoince = (req, res) => {
  let servicesandprices = req.body.servicesandprices;
  let servicesandpricesAr = [],
    servicesandpricesSt;

  servicesandprices.forEach(element => {
    servicesandpricesAr.push(JSON.parse(element));
  });

  servicesandpricesSt = JSON.stringify(servicesandpricesAr);

  models.invoince.create({
      date: req.body.date,
      inv_no: req.body.inv_no,
      date: req.body.date,
      servicesandprices: servicesandpricesSt,
      tax: req.body.tax,
      total: req.body.total,
      customerId: req.body.customerId
    }).then(() => {
      res.status(201).json({
        message: 'added',
      });
    })
    .catch((e) => {
      if (e.parent.errno == "1062") {
        res.status(409).json({
          message: 'Invoince no registered before'
        });
      } else {
        res.status(500).send();
      }
    });
}

exports.removeInvoinces = (req, res) => {
  const ids = req.query.ids;

  models.invoince.destroy({
    where: {
      id: ids
    }
  }).then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send();
  })
}

exports.updateInvoince = (req, res) => {
  const vid = req.params.id;

  let servicesandprices = req.body.servicesandprices;
  let servicesandpricesAr = [],
    servicesandpricesSt;

  servicesandprices.forEach(element => {
    servicesandpricesAr.push(JSON.parse(element));
  });

  servicesandpricesSt = JSON.stringify(servicesandpricesAr);

  models.invoince.update({
      date: req.body.date,
      inv_no: req.body.inv_no,
      date: req.body.date,
      servicesandprices: servicesandpricesSt,
      tax: req.body.tax,
      total: req.body.total,
      customerId: req.body.customerId
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(201).json({
        message: 'updated',
      });
    })
    .catch((e) => {
      if (e.parent.errno == "1062") {
        res.status(409).json({
          message: 'Invoince no registered before'
        });
      } else {
        res.status(500).send();
      }
    });
}