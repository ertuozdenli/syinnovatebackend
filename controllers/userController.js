const models = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const validator = require('validator');

exports.main = (req, res) => {
  models.user.findAll({
    include: {
      all: true
    }
  }).then((result) => {
    res.status(200).json(result);
  }).catch(() => {
    res.status(404).send();
  });
};

exports.getUser = (req, res) => {
  models.user.findAll({
    where: {
      id: req.params.id,
    },
  }).then((result) => {
    if (result !== '') {
      res.status(200).json(result);
    } else {
      res.status(404).send();
    }
  }).catch(() => {
    res.status(404).send();
  });
};

exports.addUser = (req, res) => {
  const vname = req.body.name || "";
  const vmail = req.body.email || "";
  const vpass = req.body.password || "";
  const regLowerCase = /[a-z]/;
  const regUpperCase = /[A-Z]/;
  const regNumeric = /[0-9]/;
  let err = [];

  validator.isEmail(vmail) ? '' : err.push({
    message: 'invalid email'
  });

  !validator.isEmpty(vmail) ? '' : err.push({
    message: 'email can not be null'
  });

  !validator.isEmpty(vname) ? '' : err.push({
    message: 'name can not be null'
  });

  !validator.isEmpty(vpass) ? '' : err.push({
    message: 'password can not be null'
  });

  validator.isLength(vpass, {
    min: 8
  }) ? '' : err.push({
    message: 'password length must be minimum length 8 characters'
  });

  regLowerCase.test(vpass) ? '' :
    err.push({
      message: 'password must have a lowercase character'
    });

  regUpperCase.test(vpass) ? '' :
    err.push({
      message: 'password must contain uppercase character'
    });

  regNumeric.test(vpass) ? '' :
    err.push({
      message: 'password must contain digit(s)'
    });




  if (err.length == 0) {

    bcrypt.hash(req.body.password, 10, (error, hash) => {
      if (error) {
        res.status(500).json({
          message: error,
        });
      } else {
        let data = {};
        data.name = vname;
        data.email = vmail;
        data.password = hash;
        data.role = req.body.role;

        if (req.file) {
          data.profileImg = req.file.profileImg;
        }

        models.user.create(data).then(() => {
          res.status(201).json({
            message: 'created'
          });

        }).catch((e) => {
          if (e.parent.errno == "1062") {
            res.status(409).json({
              message: 'email registered before'
            });
          } else {
            res.status(500).send(e);
          }
        });
      }
    });

  } else {
    res.status(406).json(err);
  }

};

exports.signin = (req, res) => {
  const vemail = req.body.email;
  const password = req.body.password;

  models.user.findAll({
    where: {
      email: vemail,
      status: true
    },
  }).then((result) => {
    if (result != "") {
      bcrypt.compare(password, result[0].password, (err, loginResult) => {
        if (err) {
          res.status(500).json({
            message: err.message,
          });
        } else if (loginResult) {
          const vtoken = jwt.sign({
            id: result[0].id,
            email: result[0].email,
          }, process.env.JWT_SECRET, {
            algorithm: 'HS256',
            expiresIn: '48h',
          });
          res.status(202).json({
            token: vtoken,
          });
        } else {
          res.status(401).json({
            message: 'Wrong passord or email',
          });
        }
      });
    } else {
      res.status(404).json({
        message: 'user doesn\'t exist or doesn\'t activeted yet',
      });
    }
  }).catch((e) => {
    res.status(500).json({
      message: e,
    });
  });
};

exports.updateUser = (req, res) => {
  const vid = req.params.id;

  data = {}
  data.name = req.body.name;
  data.email = req.body.email;
  data.roleId = req.body.role;
  data.status = req.body.status;

  if (req.file) {
    data.profileImg = req.file.path;
  }

  models.user
    .update(data, {
      where: {
        id: vid,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch((e) => {
      if (e.parent.errno == "1062") {
        res.status(409).json({
          message: 'email registered before'
        });
      } else {
        res.status(500).send();
      }
    });
}

exports.changePassword = (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const decoded = jwt.decode(token);
  const userId = decoded.id;

  bcrypt.hash(req.body.password, 10, (error, hash) => {
    if (error) {
      res.status(500).json({
        message: error,
      });
    } else {
      models.user.update({
        password: hash
      }, {
        where: {
          id: userId
        }
      }).then(() => {
        res.status(204).json({
          message: 'updated'
        });
      }).catch((e) => {
        res.status(500).send();
      });
    }
  });

}

exports.checkAuth = (req, res) => {
  res.status(202).send();
};