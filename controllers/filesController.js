const models = require('../models');
const fs = require('fs');
const path = require('path');

exports.getFiles = (req, res) => {
  models.file.findAll().then(result => {
    res.json(result);
  });
};

exports.addFiles = (req, res) => {
  if (req.files != '') {
    try {
      const files = req.files;
      files.forEach(file => {
        models.file.create({
          name: file.originalname,
          path: file.path,
          customerId: req.body.customerId,
        });
      });
    } catch (error) {
      res.status(500).send(error);
    }
    res.status(201).send();
  } else {
    res.status(400).json({
      message: 'you did something wrong',
    });
  }
};

exports.deleteFiles = (req, res) => {
  const ids = req.query.ids;

  function deleteFiles(ids) {
    models.file.findAll({
        where: {
          id: ids
        }
      }).then(result => {
        if (result != "") {
          const dirPath = path.dirname(path.resolve('storage'));
          let filePath = result[0].dataValues.path;

          models.file
            .destroy({
              where: {
                id: ids,
              },
            }).then(() => {
              fs.unlink(dirPath + "/" + filePath, (err) => {
                if (err) {
                  console.log(err);
                }
              });
            })
        }
      }).then(() => {
        res.status(204).send();
      })
      .catch((e) => {
        console.log(e);
        res.status(500).send();
      });
  }

  if (Array.isArray(ids)) {
    ids.forEach(id => {
      deleteFiles(id);
    });
  } else {
    deleteFiles(ids);
  }

};