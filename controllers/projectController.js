const models = require('../models');

exports.getProjects = (req, res) => {
  models.project
    .findAll({
      include: {
        all: true,
      },
    })
    .then(result => {
      res.json(result);
    });
};

exports.getProject = (req, res) => {
  // console.log(req.params.id);
  let options = {
    where: {
      id: req.params.id,
    },
  };

  if (req.query.includeFullData == 'true') {
    options.include = [{
      all: true,
    }, ];
  }
  models.project
    .findAll(options)
    .then(result => {
      if (result !== '') {
        res.status(200).json(result);
      } else {
        res.status(404).send(result);
      }
    })
    .catch(e => {
      res.status(404).send(e);
    });
};

exports.createProject = (req, res) => {
  let projectId;

  models.project
    .create({
      name: req.body.name,
      desc: req.body.desc,
      price: req.body.price,
      period: req.body.period,
      customerId: req.body.customerId,
    })
    .then(result => {
      projectId = result.id;

      req.body.serviceId.forEach(serviceId => {
        models.serviceToProject
          .create({
            projectId: projectId,
            serviceId: serviceId,
          })
          .catch(e => {
            res.status(500).send();
          });
      });
    })
    .then(() => {
      let incomesSchedules = [];
      incomesSchedules = req.body.incomesSchedules;

      incomesSchedules.forEach(data => {
        data = JSON.parse(data);
        models.incomesSchedule
          .create({
            value: data.value,
            date: data.date,
            status: data.status || false,
            projectId: projectId,
          })
          .catch(e => {
            res.status(500).send();
          });
      });
    })
    .then(() => {
      res.status(201).json({
        message: 'created',
      });
    }).catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
};

exports.updateProject = (req, res) => {
  const vid = req.params.id;

  models.project
    .update({
      name: req.body.name,
      desc: req.body.desc,
      price: req.body.price,
      period: req.body.period
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(204).send();
    })
    .catch(e => {
      res.status(500).send(e);
    });

}

exports.deleteProject = (req, res) => {
  const ids = req.query.ids;
  models.project
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(e => {
      res.status(500).send();
    });
};

exports.addServiceToProject = (req, res) => {
  const projectId = req.params.id;
  const serviceId = req.body.serviceId;

  models.serviceToProject.create({
    projectId: projectId,
    serviceId: serviceId
  }).then(() => {
    res.status(201).json({
      message: 'created',
    });
  }).catch(() => {
    res.status(500).json({
      message: 'server error',
    });
  });
}

exports.removeServiceToProject = (req, res) => {
  const projectId = req.params.projectId;
  const serviceId = req.params.serviceId;

  models.serviceToProject.destroy({
    where: {
      projectId: projectId,
      serviceId: serviceId
    }
  }).then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).json({
      message: 'server error',
    });
  });
}

exports.addIncomesSchedulesToProject = (req, res) => {
  const projectId = req.params.projectId;
  let incomesSchedules = [];
  incomesSchedules = req.body.incomesSchedules;

  incomesSchedules.forEach(data => {
    data = JSON.parse(data);
    models.incomesSchedule
      .create({
        value: data.value,
        date: data.date,
        projectId: projectId,
      }).then(() => {
        res.status(201).json({
          message: 'created',
        });
      })
      .catch(e => {
        res.status(500).send();
      });
  });
}
