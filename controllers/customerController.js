const models = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.main = (req, res) => {
  res.status(200).json({
    message: 'You can use /signup, /signin, /id for get user information',
  });
};

exports.getCustomers = (req, res) => {
  let options = {};

  if (req.query.includeFullData == 'true') {
    options.include = [{
      all: true,
    }, ];
  }
  if (req.query.limit) {
    options.limit = parseInt(req.query.limit);
  }

  if (req.query.offset) {
    options.offset = parseInt(req.query.offset);
  }

  models.customer
    .findAll(options)
    .then(result => {
      if (result !== '') {
        res.status(200).json(result);
      } else {
        res.status(404).send(result);
      }
    })
    .catch(e => {
      res.status(404).send(e);
    });
};

exports.getCustomer = (req, res) => {
  // console.log(req.params.id);
  let options = {
    where: {
      id: req.params.id,
    },
  };

  if (req.query.includeFullData == 'true') {
    options.include = [{
      all: true,
    }, ];
  }
  models.customer
    .findAll(options)
    .then(result => {
      if (result !== '') {
        res.status(200).json(result);
      } else {
        res.status(404).send(result);
      }
    })
    .catch(e => {
      res.status(404).send(e);
    });
};

exports.addCustomer = (req, res) => {
  models.customer
    .create({
      name: req.body.name,
      email: req.body.email,
      web: req.body.web,
      logo: req.body.logo,
      invoinceInfos: [{
        alias: req.body.invoinceAlias,
        name: req.body.invoinceName,
        taxOffice: req.body.invoinceTaxOffice,
        taxNo: req.body.invoinceTaxno,
        address: req.body.invoinceAddress,
      }, ],
      technicInfo: [{
        hosting: req.body.technicInfoHosting,
        domain: req.body.technicInfoDomain,
        social: req.body.technicInfoSocial,
      }, ],
    }, {
      include: [models.technicInfo, models.invoinceInfo],
    }, )
    .then(result => {
      res.status(200).send(result);
    });

  // res.send('ok');
};

exports.updateCustomer = (req, res) => {
  models.customer
    .update({
      name: req.body.name,
      email: req.body.email,
      web: req.body.web,
      logo: req.body.logo,
    }, {
      where: {
        id: req.params.id
      }
    }, )
    .then(result => {
      res.status(204).send();
    })
    .catch(e => {
      res.status(500).send(e);
    });
};

exports.deleteCustomer = (req, res) => {
  const ids = req.query.ids;
  models.customer
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(e => {
      res.status(500).send();
    });
};