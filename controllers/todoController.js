const models = require('../models');
const jwt = require('jsonwebtoken');




exports.getTodos = (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const decoded = jwt.decode(token);
  const voffset = parseInt(req.query.offset);
  let options = {};

  if (req.query.limit) {
    options.limit = parseInt(req.query.limit);
  }

  if (req.query.offset) {
    options.offset = parseInt(req.query.offset);
  }
  options.where = {
    userId: decoded.id
  };
  models.todo
    .findAll(options)
    .then((result) => {
      res.json(result);
    });
};

exports.createTodo = (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const decoded = jwt.decode(token);
  let statusv = 0;

  // if (!req.file) {
  //   console.log("triggered");
  //   res.status(400).send();
  // }

  models.todo
    .create({
      head: req.body.head,
      body: req.body.body,
      status: statusv,
      userId: decoded.id
    })
    .then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });


};

exports.getTodoDetails = (req, res) => {
  const vid = req.params.id;
  models.todo
    .findAll({
      where: {
        id: vid,
      },
    })
    .then((result) => {
      res.status(200).json(result);
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
};

exports.updateTodo = (req, res) => {
  const vid = req.params.id;
  const status = req.query.status || false;

  let data = {};

  if (status) {
    data.status = status;
  }
  if (req.body.head && req.body.body) {
    data.head = req.body.head,
      data.body = req.body.body
  }
  models.todo
    .update(data, {
      where: {
        id: vid,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.deleteTodo = (req, res) => {
  const ids = req.query.ids;
  // console.log(req.query.ids);
  models.todo
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};