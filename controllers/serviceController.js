const models = require('../models');

exports.getServices = (req, res) => {
  models.service
    .findAll({
      include: {
        all: true
      }
    })
    .then((result) => {
      res.json(result);
    });
};

exports.getService = (req, res) => {
  const vid = req.params.id;
  models.service
    .findAll({
      include: {
        all: true
      },
      where: {
        id: vid
      }
    })
    .then((result) => {
      if (result.length != 0) {
        res.json(result);
      } else {
        res.status(404).send();
      }

    }).catch(() => {
      res.status(500).send();
    });
}

exports.createService = (req, res) => {
  models.service.create({
      name: req.body.name,
      desc: req.body.desc,
      defaultPrice: req.body.price,
      taxId: req.body.taxId
    }).then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch((e) => {
      console.log(e);
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.updateService = (req, res) => {
  const vid = req.params.id;

  let data = {};
  data.name = req.body.name;
  data.desc = req.body.desc;
  data.defaultPrice = req.body.price;
  data.taxID = req.body.taxId;

  models.service
    .update(data, {
      where: {
        id: vid,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.deleteService = (req, res) => {
  const ids = req.query.ids;

  models.service
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};