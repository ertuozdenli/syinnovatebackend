const models = require('../models');

exports.getIncomes = (req, res) => {
  models.income.findAll({
    include: {
      all: true
    }
  }).then((result) => {
    res.status(200).json(result)
  }).catch(() => {
    res.status(500).send();
  })
}

exports.addIncome = (req, res) => {
  models.income.create({
      date: req.body.date,
      value: req.body.value,
      customerId: req.body.customerId
    }).then(() => {
      res.status(201).json({
        message: 'added',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.removeIncome = (req, res) => {
  const ids = req.query.ids;

  models.income.destroy({
    where: {
      id: ids
    }
  }).then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send();
  })
}

exports.updateIncome = (req, res) => {
  const vid = req.params.id;

  models.income.update({
      date: req.body.date,
      value: req.body.value,
      customerId: req.body.customerId
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}