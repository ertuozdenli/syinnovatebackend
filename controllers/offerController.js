const models = require('../models');

exports.getOffers = (req, res) => {
  models.offer.findAll({
    include: {
      all: true
    }
  }).then((result) => {
    res.status(200).json(result);
  }).catch(e => {
    res.status(500).send();
  });
}

exports.getOffer = (req, res) => {
  const vid = req.params.id;
  models.offer
    .findAll({
      where: {
        id: vid
      }
    })
    .then(result => {
      if (result !== '') {
        res.status(200).json(result);
      } else {
        res.status(404).send(result);
      }
    })
    .catch(e => {
      res.status(404).send(e);
    });
}


exports.createOffer = (req, res) => {

  let incomesSchedules = req.body.incomesSchedules;
  let incomesSchedulesData = [];
  let incomesSchedulesDataString;
  let servicesDataString = req.body.servicesData.toString();

  incomesSchedules.forEach(element => {
    incomesSchedulesData.push(JSON.parse(element));
  });
  incomesSchedulesDataString = JSON.stringify(incomesSchedulesData);

  models.offer.create({
      name: req.body.name,
      desc: req.body.desc,
      price: req.body.price,
      startTime: req.body.starttime,
      deadline: req.body.deadline,
      status: req.body.status,
      period: req.body.period,
      incomesSchedulesData: incomesSchedulesDataString,
      servicesData: servicesDataString,
      customerId: req.body.customerId
    }).then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch((e) => {
      console.log(e);
      res.status(500).json({
        message: 'server error',
      });
    });
}


exports.updateOffer = (req, res) => {
  const vid = req.params.id;
  let incomesSchedules = req.body.incomesSchedules;
  let incomesSchedulesData = [];
  let incomesSchedulesDataString;
  let servicesDataString = req.body.servicesData.toString();

  incomesSchedules.forEach(element => {
    incomesSchedulesData.push(JSON.parse(element));
  });
  incomesSchedulesDataString = JSON.stringify(incomesSchedulesData);


  models.offer.update({
      name: req.body.name,
      desc: req.body.desc,
      price: req.body.price,
      startTime: req.body.starttime,
      deadline: req.body.deadline,
      status: req.body.status,
      period: req.body.period,
      incomesSchedulesData: incomesSchedulesDataString,
      servicesData: servicesDataString
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(204).send();
    })
    .catch((e) => {
      console.log(e);
      res.status(500).json({
        message: 'server error',
      });
    });
}


exports.deleteOffer = (req, res) => {
  const ids = req.query.ids;
  models.offer
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(e => {
      res.status(500).send();
    });
};

exports.convertOfferToProject = (req, res) => {
  const vid = req.params.id;
  let offer;

  models.offer.findAll({
    where: {
      id: vid
    }
  }).then(result => {
    offer = result[0].dataValues;
    let services = offer.servicesData.split(",");
    let projectId;

    models.project
      .create({
        name: offer.name,
        desc: offer.desc,
        price: offer.price,
        period: offer.period,
        startTime: offer.startTime,
        deadline: offer.deadline,
        customerId: offer.customerId,
      })
      .then(result => {
        projectId = result.id;

        services.forEach(serviceId => {
          models.serviceToProject
            .create({
              projectId: projectId,
              serviceId: serviceId,
            })
            .catch(e => {
              res.status(500).send();
            });
        });

      })
      .then(() => {
        let incomesSchedulesData = JSON.parse(offer.incomesSchedulesData);

        incomesSchedulesData.forEach(data => {
          console.log(data);
          // data = JSON.parse(data);
          models.incomesSchedule
            .create({
              value: data.value,
              date: data.date,
              status: data.status || false,
              projectId: projectId,
            })
            .catch(e => {
              res.status(500).send();
            });

        });
      })
      .catch((e) => {
        res.status(500).json({
          message: 'server error',
        });
      });
  }).then(() => {
    // after process done update offer status
    models.offer.update({
      status: true
    }, {
      where: {
        id: vid
      }
    }).catch((e) => {
      res.status(500).json({
        message: 'server error',
      });
    });

  }).then(() => {

    res.status(201).json({
      message: 'completed',
    });

  });
}