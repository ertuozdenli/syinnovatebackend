const models = require('../models');

exports.getRoles = (req, res) => {
  models.role
    .findAll()
    .then((result) => {
      res.json(result);
    });
};

exports.createRole = (req, res) => {
  models.role.create({
      name: req.body.name,
      desc: req.body.desc
    }).then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.updateRole = (req, res) => {
  const vid = req.params.id;

  let data = {};
  data.name = req.body.name;
  data.desc = req.body.desc;

  models.todo
    .update(data, {
      where: {
        id: vid,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};