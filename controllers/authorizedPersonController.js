const models = require('../models');

exports.getAuthorizedPersons = (req, res) => {
  const customerId = req.query.cid;
  if (customerId == null) {
    res.status(400).json({
      'message': 'You must give a customer id with like "?cid=id"'
    });
  }
  models.authorizedperson
    .findAll({
      where: {
        customerId: customerId
      }
    })
    .then((result) => {
      res.json(result);
    }).catch(e => {
      res.status(500).send();
      console.log(e);
    });
};

exports.addAuthorizedPerson = (req, res) => {
  const customerId = req.body.cid;
  if (customerId == null) {
    res.status(400).json({
      'message': 'You must give a customer id with -cid- veriable'
    });
  }
  models.authorizedperson
    .create({
      title: req.body.title,
      name: req.body.name,
      phone: req.body.phone,
      mail: req.body.mail,
      customerId: customerId
    })
    .then((result) => {
      res.json(result);
    }).catch(e => {
      res.status(500).send();
      console.log(e);
    });
}

exports.deleteAuthorizedPerson = (req, res) => {
  const ids = req.query.ids;
  // console.log(req.query.ids);
  models.authorizedperson
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.updateAuthorizedPerson = (req, res) => {
  const vid = req.params.id;

  if (vid == null) {
    res.status(406).json({
      'message': 'You must give a AuthorizedPerson id'
    });
  } else {
    let data = {};
    data.title = req.body.title;
    data.name = req.body.name;
    data.phone = req.body.phone;
    data.mail = req.body.mail;

    models.authorizedperson
      .update(data, {
        where: {
          id: vid,
        },
      })
      .then(() => {
        res.status(204).send();
      })
      .catch(() => {
        res.status(500).send();
      });
  }
};