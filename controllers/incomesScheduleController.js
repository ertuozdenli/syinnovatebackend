const models = require('../models');

exports.getIncomesSchedules = (req, res) => {
  models.incomesSchedule.findAll({
    include: [models.project]
  }).then((result) => {
    res.status(200).json(result);
  }).catch(e => {
    res.status(500).send();
  });
}

exports.getIncomesSchedulesWaiting = (req, res) => {
  models.incomesSchedule.findAll({
    where: {
      status: false
    }
  }, {
    include: [models.project]
  }).then((result) => {
    res.status(200).json(result);
  }).catch(e => {
    res.status(500).send();
  });
}

exports.addIncomesDate = (req, res) => {
  models.incomesSchedule.create({
      value: req.body.value,
      date: req.body.date,
      projectId: req.body.projectId
    }).then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}