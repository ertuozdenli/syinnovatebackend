const models = require('../models');

exports.getTaxs = (req, res) => {
  models.tax
    .findAll()
    .then((result) => {
      res.json(result);
    });
};

exports.getTax = (req, res) => {
  const vid = req.params.id;
  models.tax
    .findAll({
      where: {
        id: vid
      }
    })
    .then((result) => {
      if (result.length != 0) {
        res.json(result);
      } else {
        res.status(404).send();
      }

    }).catch(() => {
      res.status(500).send();
    });
}

exports.createTax = (req, res) => {
  models.tax.create({
      name: req.body.name,
      value: req.body.value
    }).then(() => {
      res.status(201).json({
        message: 'created',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.updateTax = (req, res) => {
  const vid = req.params.id;

  let data = {};
  data.name = req.body.name;
  data.value = req.body.value;

  models.tax
    .update(data, {
      where: {
        id: vid,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.deleteTaxs = (req, res) => {
  const ids = req.query.ids;

  models.tax
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
};