const models = require('../models');

exports.getInvoinceInfos = (req, res) => {
  const customerId = req.query.cid;
  if (customerId == null) {
    res.status(400).json({
      'message': 'You must give a customer id with like "?cid=id"'
    });
  }
  models.invoinceInfo
    .findAll({
      where: {
        customerId: customerId
      }
    })
    .then((result) => {
      res.json(result);
    }).catch(e => {
      res.status(500).send();
    });
};

exports.createInvoinceInfo = (req, res) => {
  const cid = req.body.cid;
  if (cid == null) {
    res.status(406).json({
      'message': 'You must give a customer id'
    });
  } else {
    models.invoinceInfo.create({
      alias: req.body.alias,
      name: req.body.name,
      taxOffice: req.body.taxOffice,
      taxNo: req.body.taxNo,
      address: req.body.address,
      customerId: cid
    }).then(result => {
      res.status(201).json({
        message: 'created',
      });
    }).catch(e => {
      res.status(500).json({
        message: 'server error',
      });
    })
  }

}


exports.updateInvoinceInfo = (req, res) => {
  const vid = req.params.id;
  
  if (vid == null) {
    res.status(406).json({
      'message': 'You must give a invoinceinfo id'
    });
  } else {
    let data = {};
    data.alias = req.body.alias;
    data.name = req.body.name;
    data.taxOffice = req.body.taxOffice;
    data.taxNo = req.body.taxNo;
    data.address = req.body.address;
    models.invoinceInfo
      .update(data, {
        where: {
          id: vid,
        },
      })
      .then(() => {
        res.status(204).send();
      })
      .catch(() => {
        res.status(500).send();
      });
  }
};

exports.deleteInvoinceInfo = (req, res) => {
  const ids = req.params.id;
  models.invoinceInfo
    .destroy({
      where: {
        id: ids,
      },
    })
    .then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).send();
    });
}