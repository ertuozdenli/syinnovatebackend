const models = require('../models');

exports.getExpenses = (req, res) => {
  models.expense.findAll().then((result) => {
    res.status(200).json(result)
  }).catch(() => {
    res.status(500).send();
  })
}

exports.addExpense = (req, res) => {
  models.expense.create({
      towho: req.body.towho,
      desc: req.body.desc,
      value: req.body.value,
      date: req.body.date,
      value: req.body.value,
      status: req.body.status
    }).then(() => {
      res.status(201).json({
        message: 'added',
      });
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.removeExpense = (req, res) => {
  const ids = req.query.ids;

  models.expense.destroy({
    where: {
      id: ids
    }
  }).then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send();
  })
}

exports.updateExpense = (req, res) => {
  const vid = req.params.id;

  models.expense.update({
      towho: req.body.towho,
      desc: req.body.desc,
      value: req.body.value,
      date: req.body.date,
      value: req.body.value,
      status: req.body.status
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}

exports.updateStatus = (req, res) => {
  const vid = req.params.id;
  models.expense.update({
      status: req.body.status
    }, {
      where: {
        id: vid
      }
    }).then(() => {
      res.status(204).send();
    })
    .catch(() => {
      res.status(500).json({
        message: 'server error',
      });
    });
}