'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('invoinces', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      inv_no: {
        type: Sequelize.INTEGER,
        unique: true
      },
      date: {
        type: Sequelize.DATE
      },
      servicesandprices: {
        type: Sequelize.STRING
      },
      tax: {
        type: Sequelize.DECIMAL(10, 2)
      },
      total: {
        type: Sequelize.DECIMAL(10, 2)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('invoinces');
  }
};